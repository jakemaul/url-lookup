# About #

OpenDNS "URL Lookup" Web Service

This app is built in Python using Flask, and relies on SQLAlchemy for database interactions. Many different databases should work, but I've tested local SQLite files (not recommended for production, but handy for testing) and MySQL. In order to install dependencies, you'll need the relevant binary libraries for Python and your database of choice. The requirements.txt assumes MySQL.

We also use Memcache between the app and the database for faster access to recently-used queries.

# Preparing to Run #

To get this app up and running:

    virtualenv venv
    cd venv
    git clone https://jakemaul@bitbucket.org/jakemaul/url-lookup.git
    cd url-lookup
    pip install -r lookup/requirements.txt

(Edit lookup/settings.py as needed for database location and credentials)
The app has split logic for handling database reads vs writes- be sure both DATABASE and RO_DATABASE settings are set properly! They can be set to the same value if you don't have a replicating database.

Initialize the database:

    python
    >> from lookup.database import init_db
    >> init_db()

## Using Docker ##

You can also launch this app as a Docker container:

    git clone https://jakemaul@bitbucket.org/jakemaul/url-lookup.git
    cd url-lookup
    docker build -t my-python-app .
    docker run -it --rm -p 8000:80 --name my-running-app my-python-app

Note, you will still need to configure lookup/settings.py properly for your environment. When launched, the app will be accessible on port 8000.

# Running #

Run the app:

    python runserver.py

-or-

    gunicorn lookup:app

-or-

Apache/mod_wsgi using lookup.wsgi, enter the following in the relevant VirtualHost container:

    WSGIDaemonProcess lookup
    WSGIScriptAlias / /path/to/lookup.wsgi


# Benchmark test results (no memcache) #

Using "apachebench", running the test on the same node as the app *and* the database.

    4 gunicorn workers
    10,000 requests, concurrency 4
    
    Server Software:        gunicorn/19.3.0
    Server Hostname:        127.0.0.1
    Server Port:            8000
    
    Document Path:          /urlinfo/1/linkedin.com/file/to/test?auth=1
    Document Length:        111 bytes
    
    Concurrency Level:      4
    Time taken for tests:   11.341 seconds
    Complete requests:      10000
    Failed requests:        0
    Total transferred:      2910000 bytes
    HTML transferred:       1110000 bytes
    Requests per second:    881.73 [#/sec] (mean)
    Time per request:       4.537 [ms] (mean)
    Time per request:       1.134 [ms] (mean, across all concurrent requests)
    Transfer rate:          250.57 [Kbytes/sec] received
    
    Connection Times (ms)
                  min  mean[+/-sd] median   max
    Connect:        0    0   2.2      0     127
    Processing:     3    4   6.8      3     130
    Waiting:        2    4   6.8      3     130
    Total:          3    4   7.1      4     131
    
    Percentage of the requests served within a certain time (ms)
      50%      4
      66%      4
      75%      4
      80%      4
      90%      5
      95%      5
      98%      6
      99%      6
     100%    131 (longest request)

# Benchmark results (with memcache) #

This makes the same request over and over, so virtually every query is responds from memcache. The above result is pessimistic (all misses), this one is optimistic (all hits).

    4 gunicorn workers
    10,000 requests, concurrency 4
    
    Server Software:        gunicorn/19.3.0
    Server Hostname:        127.0.0.1
    Server Port:            8000
    
    Document Path:          /urlinfo/1/linkedin.com/file/to/test?auth=1
    Document Length:        111 bytes
    
    Concurrency Level:      4
    Time taken for tests:   4.156 seconds
    Complete requests:      10000
    Failed requests:        0
    Total transferred:      2920000 bytes
    HTML transferred:       1110000 bytes
    Requests per second:    2406.08 [#/sec] (mean)
    Time per request:       1.662 [ms] (mean)
    Time per request:       0.416 [ms] (mean, across all concurrent requests)
    Transfer rate:          686.11 [Kbytes/sec] received
    
    Connection Times (ms)
                  min  mean[+/-sd] median   max
    Connect:        0    0   0.1      0       8
    Processing:     1    2   1.2      1      49
    Waiting:        1    1   1.2      1      48
    Total:          1    2   1.2      2      49
    
    Percentage of the requests served within a certain time (ms)
      50%      2
      66%      2
      75%      2
      80%      2
      90%      2
      95%      2
      98%      2
      99%      2
     100%     49 (longest request)

# Advice on Scaling #

Increase the number of workers per node/container until CPU usage is at the desired level. That is, until adding more workers decreases response time or throughput. A good starting point would be anywhere from 1 to 2 workers per CPU core available. This is done on the command line (ex: "gunicorn -w 4 lookup:app") or in the Apache config (ex: "WSGDaemonProcess lookup processes=2 threads=15").

Be sure to set up a Memcache server and configure it in lookup/settings.py. The app will work without one, but performance is significantly better with one!

Place multiple nodes/containers behind a load balancer. (ex: Nginx, Varnish, Riverbed Stingray, F5 LTM, HAproxy, IPVS/LVS)

Use a full-page cache. Often this sort of cache can also do load balancing (such as Varnish or Nginx), so consider combining this role with the load balancer role to reduce overall complexity.

Consider changing CACHE_CONTROL in lookup/settings.py. A higher max-age value will increase the hit rate on any full-page cache in front of the app servers. Higher values may cause a delay between when new data is entered and when it becomes visible.

If MySQL performance is an issue, consider increasing MEMCACHE_TTL in lookup/settings.py. This will reduce the query volume sent to the database. Higher values may cause a delay between when new data is entered and when it becomes visible.

Use a MySQL read-only slave for read queries.

Use multiple MySQL read-only slaves for read queries, placed behind a load balancer.

Use a system like New Relic for measuring overall performance. Note that this can have an adverse affect on performance at very high throughput, so this may be something you want to restrict to only a subset of the infrastructure, or only a small sample of the incoming traffic. This can tell you whether you need additional app resources, or additional database resources.

Depending on the dataset, it may be useful to add an index on the 'hostname' column to speed up SELECT queries.

Consider using a CDN. This will increase responsiveness for those users who are geographically far away from your datacenter.

Consider using DNS to distribute queries to multiple instances of the application and database spread around the world. Similar to a CDN, this will increase responsiveness for users worldwide, but in addition it will also provide a heavy degree of redundancy by decentralizing the "authoritative response" across many datacenters/POPs.
