from flask import Flask
app = Flask(__name__)

app.config.from_object('lookup.settings')
#app.config.from_envvar('URLLOOKUP_SETTINGS')

import lookup.views
