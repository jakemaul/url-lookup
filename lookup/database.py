from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

from lookup import app

engine = create_engine(app.config['DATABASE'], convert_unicode=True)
db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=engine))

engine_ro = create_engine(app.config['RO_DATABASE'], convert_unicode=True)
db_session_ro = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=engine_ro))
Base = declarative_base()
Base.query = db_session.query_property()
Base_ro = declarative_base()
Base_ro.query = db_session_ro.query_property()

def init_db():
    # import all modules here that might define models so that
    # they will be registered properly on the metadata.  Otherwise
    # you will have to import them first before calling init_db()
    import lookup.models
    Base.metadata.create_all(bind=engine)
