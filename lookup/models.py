from sqlalchemy import Column, Integer, String
from lookup.database import Base
from lookup.database import Base_ro

class Entry(Base):
    __tablename__ = 'entries'
    id = Column(Integer, primary_key=True)
    hostname = Column(String(50), unique=False)
    path = Column(String(120), unique=False)
    querystring = Column(String(120), unique=False)

    def __init__(self, hostname=None, path=None, querystring=None):
        self.hostname = hostname
        self.path = path
        self.querystring = querystring

#    def __repr__(self):
#        return "<Entry(hostname='%s', path='%s', querystring='%s'>" % (self.hostname, self.path, self.querystring)

class Entry_ro(Base_ro):
    __tablename__ = 'entries'
    id = Column(Integer, primary_key=True)
    hostname = Column(String(50), unique=False)
    path = Column(String(120), unique=False)
    querystring = Column(String(120), unique=False)

    def __repr__(self):
        return "<Entry(hostname='%s', path='%s', querystring='%s'>" % (self.hostname, self.path, self.querystring)

