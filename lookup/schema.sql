drop table if exists entries;
create table entries (
  id integer primary key autoincrement,
  hostname text not null,
  path text not null,
  querystring text
);
