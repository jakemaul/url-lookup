# type://user:pass@host:port/database
DATABASE      = 'mysql://urllookup:whatever@192.168.1.64/urllookup'
RO_DATABASE   = 'mysql://urllookup:whatever@192.168.1.64:3307/urllookup'

# host:port
MEMCACHE_HOST = '127.0.0.1:11211'

# in seconds
MEMCACHE_TTL  = 5

# literal string used for outgoing Cache-Control headers on lookup requests
CACHE_CONTROL = 'max-age: 10'

# user / pass for entering new entries
USERNAME      = 'admin'
PASSWORD      = 'lolwat'

# Miscellaneous settings
DEBUG         = True
SECRET_KEY    = 'sdlhfa83hwasf'
