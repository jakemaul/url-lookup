from lookup import app
import sqlite3
from flask import Flask, request, g, render_template, session, redirect, url_for, abort, flash, jsonify, make_response
from contextlib import closing

from werkzeug.contrib.cache import MemcachedCache
cache = MemcachedCache([app.config['MEMCACHE_HOST']])



# SQLAlchemy
from lookup.database import db_session
from lookup.database import db_session_ro
from lookup.models import Entry
from lookup.models import Entry_ro

@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()
    db_session_ro.remove()


@app.route('/')
def show_entries():
    entries = Entry_ro.query.all()
    return render_template('show_entries.html', entries=entries)

@app.route("/urlinfo/1/<hostname>/<path:path>")
def lookup(hostname,path):
    querystring = "?" + request.query_string
    path = "/" + path
    test = hostname + path + querystring
    mc = cache.get(test)
    if mc is not None:
        return mc
    e = Entry_ro.query.filter(Entry_ro.hostname == hostname, Entry_ro.path == path, Entry_ro.querystring == querystring).first()
    if e is None:
        status = "not found"
    else:
        status = "found"
    resp = make_response( jsonify(hostname=hostname, path=path, querystring=querystring, status=status) )
    resp.headers['Cache-Control'] = app.config['CACHE_CONTROL']
    cache.set(test, resp, timeout=app.config['MEMCACHE_TTL'])
    return resp


@app.route("/urlload/1/", methods=['POST'])
def load():
    e = Entry(request.form['hostname'], request.form['path'], request.form['querystring'])
    db_session.add(e)
    db_session.commit()
    flash('New entry was successfully posted')
    return redirect(url_for('show_entries'))

@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        if request.form['username'] != app.config['USERNAME']:
            error = 'Invalid username'
        elif request.form['password'] != app.config['PASSWORD']:
            error = 'Invalid password'
        else:
            session['logged_in'] = True
            flash('You were logged in')
            return redirect(url_for('show_entries'))
    return render_template('login.html', error=error)

@app.route('/logout')
def logout():
    session.pop('logged_in', None)
    flash('You were logged out')
    return redirect(url_for('show_entries'))


if __name__ == "__main__":
    app.run(host='0.0.0.0',debug=True)
