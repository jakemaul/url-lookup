import os
import lookup
import unittest
import tempfile

class LookupTestCase(unittest.TestCase):

    # this doesn't quite work:
    # it uses the thing defined in URLLOOKUP_SETTINGS
    # regardless of this mkstemp() stuff here
    def setUp(self):
        self.db_fd, filename = tempfile.mkstemp()
        lookup.app.config['DBFILENAME'] = filename
        lookup.app.config['DATABASE'] = 'sqlite:////' + filename
        #print(lookup.app.config['DATABASE'])
        lookup.app.config['TESTING'] = True
        self.app = lookup.app.test_client()
        lookup.database.init_db()

    def tearDown(self):
        os.close(self.db_fd)
        os.unlink(lookup.app.config['DBFILENAME'])

    def login(self, username, password):
        return self.app.post('/login', data=dict(
            username=username,
            password=password
        ), follow_redirects=True)
    
    def logout(self):
        return self.app.get('/logout', follow_redirects=True)
        

    def test_empty_db(self):
        rv = self.app.get('/')
        assert 'No entries here so far' in rv.data

    def test_login_logout(self):
        rv = self.login('admin', 'lolwat')
        assert 'You were logged in' in rv.data
        rv = self.logout()
        assert 'You were logged out' in rv.data
        rv = self.login('adminx', 'default')
        assert 'Invalid username' in rv.data
        rv = self.login('admin', 'defaultx')
        assert 'Invalid password' in rv.data

    def test_load(self):
        self.login('admin', 'lolwat')
        rv = self.app.post('/urlload/1/', data=dict(
            hostname='test.example.com',
            path='/something',
            querystring='?umm=1'
        ), follow_redirects=True)
        assert 'No entries here so far' not in rv.data
        assert 'test.example.com' in rv.data
        assert '/something' in rv.data
        assert '?umm=1' in rv.data

    def test_lookup(self):
        rv = self.app.get('/urlinfo/1/test.example.com/something?umm=1')
        assert 'test.example.com' in rv.data
        assert 'notfound.example.com' not in rv.data

if __name__ == '__main__':
    unittest.main()


